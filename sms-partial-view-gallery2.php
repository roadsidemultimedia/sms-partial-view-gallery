<?php
/*
Plugin Name: Partial View Gallery
Plugin URI: http://www.roadsidemultimedia.com
Description: Partial View Gallery, Uses master slider technology
Author: Curtis Grant
PageLines: true
Version: 1.0
Section: true
Class Name: PartialViewGallery
Filter: slider, gallery, roadside, full-width
Loading: active
Text Domain: dms-sms
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-partial-view-gallery
Bitbucket Branch: master
*/
if( ! class_exists( 'PageLinesSectionFactory' ) )
  return;

global $pv_slider_num;
$pv_slider_num = 0;

class PartialViewGallery extends PageLinesSection {

	var $pvdefault_limit = 3;
	var $pvdefault_style = "default";
	

	function section_styles(){
		
		wp_enqueue_style( 'masterslider-base', $this->base_url.'/style/masterslider.css');
		wp_enqueue_style( 'masterslider-skin', $this->base_url.'/skins/style.css');
		wp_enqueue_style( 'masterslider-partialview', $this->base_url.'/style/ms-partialview.css');


		wp_enqueue_script('masterslider-js', $this->base_url.'/js/masterslider.min.js', array('jquery'));
		wp_enqueue_script('masterslider-partialview-js', $this->base_url.'/js/masterslider.partialview.js', array('jquery'));
		wp_enqueue_script('masterslider-easing', $this->base_url.'/js/jquery.easing.min.js', array('jquery'));

		
	}
	
	function section_head(){

	}

	function section_opts(){
		$options = array();

		$options[] = array(

			'title' => __( 'Gallery Configuration', 'pagelines' ),
			'type'	=> 'multi',
			'col'   => 1,
			'opts'	=> array(
				array(
					'key'			=> 'pvcaption_style',
					'type' 			=> 'select',
					'label' 	=> __( 'Type your Style: overlay', 'pagelines' ),
					'opts'			=> array(
							'layered'		=> array('name' => 'Layered'),
							'overlay'		=> array('name' => 'Bar'),
						)
				),
				array(
					'key'			=> 'pvcaption_border_color',
					'type' 			=> 'color',
					'label' 	=> __( 'Select your bar color.', 'pagelines' ),
				),
				array(
					'key'			=> 'pvcaption_border_font_color',
					'type' 			=> 'color',
					'label' 	=> __( 'Select your bar font color.', 'pagelines' ),
				),
			)

		);

		$options[] =
      array(
        'key'   => 'array',
        'type'    => 'accordion', 
        'col'   => 2,
        'title' => 'Slides',
        'opts_cnt'  => 3,
        'title'   => __('Slides', 'pagelines'), 
        'opts'  => array(
          array(
				'key'		=> 'pvgallery_image',
				'label'		=> __( 'Slide Image', 'pagelines' ),
				'type'		=> 'image_upload',
			),
          array(
				'key'		=> 'pvgallery_url',
				'label'	=> __( 'Slide Video URL (Vimeo or Youtube)', 'pagelines' ),
				'type'	=> 'text', 
				'help'	=> __('<strong>Ex.</strong><br/ > http://www.youtube.com/watch?v=rNYiWNLtk5A <br />
				http://vimeo.com/6763069', 'pagelines')
			),
          array(
					'key'		=> 'pvslide_header',
					'label'		=> __( 'Heading ', 'pagelines' ),
					'type'		=> 'text',
			),
          array(
					'key'		=> 'pvslide_caption',
					'label'		=> __( 'Caption ', 'pagelines' ),
					'type'		=> 'text',
			),
          array(
					'key'		=> 'pvslide_cta',
					'label'		=> __( 'CTA Text ', 'pagelines' ),
					'type'		=> 'text',
			),
          array(
					'key'		=> 'pvslide_cta_link',
					'label'		=> __( 'CTA LINK', 'pagelines' ),
					'type'		=> 'text',
			)
          )
      );
		

		return $options;
	}
	
	function get_slides( $array ){
    
    $pvs_style = $this->opt('pvcaption_style') ? $this->opt('pvcaption_style') : 'overlay';
    $out = '';
    $num = 1;
    $nav[] = array();
    $slides[] = array();
    if( is_array( $array ) ){
      foreach( $array as $key => $item ){
        $pvsimg = pl_array_get( 'pvgallery_image', $item );
        $pvsurl = pl_array_get( 'pvgallery_url', $item );
        $pvshdr = pl_array_get( 'pvslide_header', $item );
        $pvscaption = pl_array_get( 'pvslide_caption', $item );
        $pvscta = pl_array_get( 'pvslide_cta', $item );
        $pvsctalink = pl_array_get( 'pvslide_cta_link', $item );
        $pvvid_url = (isset( $pvsurl ) &&  $pvsurl != '') ? sprintf('<a href="%s"data-type="video"> video </a>', $pvsurl) : '';
        
        if( $pvsimg ){
         
          $out .= sprintf(
            '<div class="ms-slide">
				        <img src="/img/blank.gif" data-src="%s" alt="%s"/>  
				        <div class="ms-slide-info">
				        <div class="ms-info">
				        	<h3>%s</h3>
				        	<p>%s</p>
			        	</div>
			        	</div>
			        	%s 
				    </div>',
            $pvsimg,
            $pvshdr,
            $pvshdr,
            $pvscaption,
            $pvvid_url
          );
      }

      $num++;
      }
    }
    
    
    return $out;
  }

   function section_template( ) {	

   	function hex2rgb($hex) {
   		$hex = str_replace("#", "", $hex);

    if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
    	} else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
    	}
      $rgb = array($r, $g, $b);
   		//return implode(",", $rgb); // returns the rgb values separated by commas
      return $rgb; // returns an array with the rgb values
    }

	 $pvs_style = $this->opt('pvcaption_style') ? $this->opt('pvcaption_style') : 'overlay';
	 $pvcaption_border_color = $this->opt('pvcaption_border_color') ? $this->opt('pvcaption_border_color') : '#FFF';
	 $pvcaption_border_font_color = $this->opt('pvcaption_border_font_color') ? $this->opt('pvcaption_border_font_color') : '#FFF';
	 $rgb = hex2rgb("$pvcaption_border_color");
	 $r = $rgb[0];
	 $g = $rgb[1];
	 $b = $rgb[2];
	global $pv_slider_num;
   if(!$pv_slider_num) {
     $pv_slider_num = 1;
   }
	?>
			<script>
		 jQuery(document).ready(function(jQuery) {
		 		var slider = new MasterSlider();
		         slider.setup('masterslider' , {
		        	width:760,
					height:400,
					space:0,
					loop:true,
					view:'prtialwave2'
				 });
		
				slider.control('arrows');	
		slider.control('slideinfo',{insertTo:"#partial-view-1" , autohide:false});
		slider.control('circletimer' , {color:"#FFFFFF" , stroke:9});
          })
		 jQuery(document).ready(function($) {
		 $(".ms-info").css("background", "rgba(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b; ?>,.7)");
		 $("h3.ms-layer").css("color", "#<?php echo $pvcaption_border_font_color; ?>");
		 $("p.ms-layer").css("color", "#<?php echo $pvcaption_border_font_color; ?>");
		})
		</script>
	<div class="ms-partialview-template" id="partial-view-1">
				<!-- masterslider -->
				<div class="master-slider ms-skin-default" id="masterslider">

			<?php

    $out = $this->get_content( $array );

    echo $out;

    ?>
		</div>
	</div>

<?php

$pv_slider_num++;
}


}