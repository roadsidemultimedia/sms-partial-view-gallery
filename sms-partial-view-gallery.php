<?php
	/*
Plugin Name: Partial View Gallery
Plugin URI: http://www.roadsidemultimedia.com
Description: Partial View Gallery, Uses master slider technology
Author: Curtis Grant
PageLines: true
Version: 1.0
Section: true
Class Name: PartialViewGallery
Filter: slider, gallery, roadside, full-width
Loading: active
Text Domain: dms-sms
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-partial-view-gallery
Bitbucket Branch: master
*/
if( ! class_exists( 'PageLinesSectionFactory' ) )
  return;
  
global $pv_slider_num;
$pv_slider_num = 0;

class PartialViewGallery extends PageLinesSection {

	var $pvdefault_limit = 3;
	var $pvdefault_style = "default";
	

	function section_styles(){
		
		wp_enqueue_style( 'masterslider-base', $this->base_url.'/style/masterslider.css');
		wp_enqueue_style( 'masterslider-skin', $this->base_url.'/skins/style.css');
		wp_enqueue_style( 'masterslider-partialview', $this->base_url.'/style/ms-partialview.css');


		wp_enqueue_script('masterslider-js', $this->base_url.'/js/masterslider.min.js', array('jquery'));
		wp_enqueue_script('masterslider-partialview-js', $this->base_url.'/js/masterslider.partialview.js', array('jquery'));
		wp_enqueue_script('masterslider-easing', $this->base_url.'/js/jquery.easing.min.js', array('jquery'));

		
	}
	
	function section_head(){

	}

	function section_opts(){
		$options = array();

		$options[] = array(

			'title' => __( 'Gallery Configuration', 'pagelines' ),
			'type'	=> 'multi',
			'opts'	=> array(
				array(
					'key'			=> 'pvgallery_count',
					'type' 			=> 'count_select',
					'count_start'	=> 1,
					'count_number'	=> 20,
					'default'		=> 3,
					'label' 	=> __( 'Number of Items to Configure', 'pagelines' ),
				),
				array(
					'key'			=> 'pvcaption_style',
					'type' 			=> 'select',
					'label' 	=> __( 'Type your Style: overlay', 'pagelines' ),
					'opts'			=> array(
							'layered'		=> array('name' => 'Layered'),
							'overlay'		=> array('name' => 'Bar'),
						)
				),
				array(
					'key'			=> 'pvcaption_border_color',
					'type' 			=> 'color',
					'label' 	=> __( 'Select your bar color.', 'pagelines' ),
				),
				array(
					'key'			=> 'pvcaption_border_font_color',
					'type' 			=> 'color',
					'label' 	=> __( 'Select your bar font color.', 'pagelines' ),
				),
			)

		);

		$pvslides = ($this->opt('pvgallery_count')) ? $this->opt('pvgallery_count') : $this->pvdefault_limit;
		
	
		for($i = 1; $i <= $pvslides; $i++){

			$opts = array();
			
			$opts[] = array(
				'key'		=> 'pvgallery_image_'.$i,
				'label'		=> __( 'Slide Image', 'pagelines' ),
				'type'		=> 'image_upload',
			);
			$opts[] = array(
				'key'		=> 'pvgallery_url_'.$i,
				'label'	=> __( 'Slide Video URL (Vimeo or Youtube)', 'pagelines' ),
				'type'	=> 'text', 
				'help'	=> __('<strong>Ex.</strong><br/ > http://www.youtube.com/watch?v=rNYiWNLtk5A <br />
				http://vimeo.com/6763069', 'pagelines')
			);
			$opts[] = array(
					'key'		=> 'pvslide_header_'.$i,
					'label'		=> __( 'Heading ', 'pagelines' ),
					'type'		=> 'text',
			);
			$opts[] = array(
					'key'		=> 'pvslide_caption_'.$i,
					'label'		=> __( 'Caption ', 'pagelines' ),
					'type'		=> 'text',
			);
			$opts[] = array(
					'key'		=> 'pvslide_cta_'.$i,
					'label'		=> __( 'CTA Text ', 'pagelines' ),
					'type'		=> 'text',
			);
			$opts[] = array(
					'key'		=> 'pvslide_cta_link_'.$i,
					'label'		=> __( 'CTA LINK', 'pagelines' ),
					'type'		=> 'text',
			);
			$options[] = array(
				'title' 	=> __( 'Gallery Item ', 'pagelines' ) . $i,
				'type' 		=> 'multi',
				'opts' 		=> $opts,

			);
			

		}

		return $options;
	}
	
	function the_media(){
		
		$pvnum = ($this->opt('pvgallery_count')) ? $this->opt('pvgallery_count') : $this->pvdefault_limit;
		$out = array();
		
		for($i = 1; $i <= $pvnum; $i++):
			
			$pvtitle = ($this->opt('pvgallery_title_'.$i)) ? $this->opt('pvgallery_title_'.$i) : ''; 
			$pvurl = ($this->opt('pvgallery_url_'.$i)) ? $this->opt('pvgallery_url_'.$i) : '';
			$pvimg = ($this->opt('pvgallery_image_'.$i)) ? $this->opt('pvgallery_image_'.$i) : $this->base_url.'/img/bg4.jpg';
			$pvshdr = ($this->opt('pvslide_header_'.$i)) ? $this->opt('pvslide_header_'.$i) : 'CAPTIVATING HEADING';
            $pvscaption = ($this->opt('pvslide_caption_'.$i)) ? $this->opt('pvslide_caption_'.$i) : 'caption text goes here'; 
            $pvcta = ($this->opt('pvslide_cta_'.$i)) ? $this->opt('pvslide_cta_'.$i) : 'Learn More';
            $pvctalink = ($this->opt('pvslide_cta_link_'.$i)) ? $this->opt('pvslide_cta_link_'.$i) : 'http://www.google.com'; 
			

			if($pvurl != '' || $pvimg != '' || $pvshdr != ''){
				$out[] = array(
					'pvtitle'	=> $pvtitle, 
					'pvurl'	=> $pvurl, 
					'pvimg'	=> $pvimg,
					'pvshdr'	=> $pvshdr,
					'pvscaption' => $pvscaption,
					'pvs_style' => $pvs_style,
					'pv_cta' => $pvcta,
					'pv_cta_link' => $pvctalink
				);
			}
			 
			
		endfor;
		
		return $out;
	}

   function section_template( ) {	

   	function hex2rgb($hex) {
   		$hex = str_replace("#", "", $hex);

    if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
    	} else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
    	}
      $rgb = array($r, $g, $b);
   		//return implode(",", $rgb); // returns the rgb values separated by commas
      return $rgb; // returns an array with the rgb values
    }

	 $pvs_style = $this->opt('pvcaption_style') ? $this->opt('pvcaption_style') : 'overlay';
	 $pvcaption_border_color = $this->opt('pvcaption_border_color') ? $this->opt('pvcaption_border_color') : '#FFF';
	 $pvcaption_border_font_color = $this->opt('pvcaption_border_font_color') ? $this->opt('pvcaption_border_font_color') : '#FFF';
	 $rgb = hex2rgb("$pvcaption_border_color");
	 $r = $rgb[0];
	 $g = $rgb[1];
	 $b = $rgb[2];
	global $pv_slider_num;
   if(!$pv_slider_num) {
     $pv_slider_num = 1;
   }
	?>
			<script>
		 jQuery(document).ready(function(jQuery) {
		 		var slider = new MasterSlider();
		         slider.setup('masterslider' , {
		        	width:760,
					height:400,
					space:0,
					loop:true,
					view:'prtialwave2'
				 });
		
				slider.control('arrows');	
		slider.control('slideinfo',{insertTo:"#partial-view-1" , autohide:false});
		slider.control('circletimer' , {color:"#FFFFFF" , stroke:9});
          })
		 jQuery(document).ready(function($) {
		 $(".ms-info").css("background", "rgba(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b; ?>,.7)");
		 $("h3.ms-layer").css("color", "#<?php echo $pvcaption_border_font_color; ?>");
		 $("p.ms-layer").css("color", "#<?php echo $pvcaption_border_font_color; ?>");
		})
		</script>
	<div class="ms-partialview-template" id="partial-view-1">
				<!-- masterslider -->
				<div class="master-slider ms-skin-default" id="masterslider">

			<?php foreach($this->the_media() as $pv): 

			$pvvid_url = (isset( $pv['pvurl'] ) &&  $pv['pvurl'] != '') ? sprintf('<a href="%s"data-type="video"> video </a>', $pv['pvurl']) : '';
			?>

				<!-- new slide -->
				<div class="ms-slide">
				        <img src="/img/blank.gif" data-src="<?php echo $pv['pvimg']; ?>" alt="lorem ipsum dolor sit"/>  
				        <?php if($pvs_style=="overlay") { ?>
				        <div class="ms-slide-info">
				        <div class="ms-info">
				        	<h3 style="color:<?php echo $pvcaption_border_font_color; ?> !important;"><?php echo $pv['pvshdr']; ?></h3>
				        	<p style="color:<?php echo $pvcaption_border_font_color; ?> !important;"><?php echo $pv['pvscaption']; ?></p>
			        	</div>
			        	</div>
			        	<?php } else { ?> 
			        	<h3 class="ms-layer light-title" style="left:80px; top:50px; width:100%; color:<?php echo $pvcaption_border_font_color; ?> !important;" 
				        	data-effect="left(short)"
				        	data-duration="1500"
				        	data-delay="10"
				        	data-ease="easeOutExpo"
				        >DOLOR SIT AMET</h3>
				        
				        <p class="ms-layer normal-desc"  style="left:80px; top:120px; width:300px; color:<?php echo $pvcaption_border_font_color; ?> !important;" 
				        	data-effect="bottom(40)"
				        	data-duration="1500"
				        	data-delay="10"
				        	data-ease="easeOutExpo"
				        >Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p>
			        	<?php } ?> 
			        	<?php echo $pvvid_url; ?> 
				    </div>
                <!-- end of slide -->
			<?php endforeach; ?>
		</div>
	</div>

<?php

$pv_slider_num++;
}


}